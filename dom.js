let listGroupEl = document.querySelector("#items");
let inputTextEl = document.getElementById("item");
let items = listGroupEl.getElementsByTagName("li");
let filterEl = document.getElementById("filter");
let formEl = document.getElementById("addForm");
let data = [];

//accessing data from localStorage when page reload

if (localStorage.data) {
  if (localStorage.getItem("data") != null) {
    data = localStorage.getItem("data");
    data = JSON.parse(data);
    if (!Array.isArray(data)) {
      localStorage.setItem("data", "[]");
      data = localStorage.getItem("data")
      data = JSON.parse(data)
    }
  }
} else {
  data = JSON.stringify(data);
  localStorage.setItem("data", data);
}

//creating listItems from localStorage

if (localStorage.getItem("data") != null) {
  let data = JSON.parse(localStorage.getItem("data"));
  if (data.length > 0) {
    for (let each of data) {
      let li = document.createElement("li");
      li.className = "list-group-item";
      li.textContent = each.value;
      let buttonEl = document.createElement("button");
      buttonEl.className = "btn btn-danger btn-sm float-right delete";
      buttonEl.textContent = "X";
      li.appendChild(buttonEl);
      listGroupEl.appendChild(li);
    }
  }
}

//create a new item in the list

formEl.addEventListener("submit", createlistEl);

function createlistEl(e) {
  e.preventDefault();
  let inputObject = { input: "", value: "" };
  let newText = inputTextEl.value;

  if (newText == "") {
    alert("Please give an input to create new list item"); //alert message to give an input value
  } else {
    let isItemExists = false;

    //checking item exists in the list or not
    if (data != []) {
      console.log(data)
      for (let each of data) {
        if (each.value.toLowerCase() == newText.toLowerCase()) {
          isItemExists = true;
        }
      }
    }

    //display alert message to avoid duplicate items
    if (isItemExists) {
      inputTextEl.value = "";
      alert(`${newText} already exists in the list.`);
    } else {
      inputTextEl.value = "";
      let inputObject = { input: "", value: "" };
      let listEl = document.createElement("li");
      listEl.className = "list-group-item";
      listEl.textContent = newText;
      if (data.length > 0) {
        let lastElementInputValue = data[data.length - 1].input;
        let newInputValue = lastElementInputValue + 1;
        inputObject.input = newInputValue;
        inputObject.value = newText;
        console.log(data);
        data.push(inputObject);
      } else {
        inputObject.input = 1;
        inputObject.value = newText;
        data.push(inputObject);
      }

      //store new item in localStorage
      localStorage.setItem("data", JSON.stringify(data));
      let deleteButtonEl = document.createElement("button");
      deleteButtonEl.className = "btn btn-danger btn-sm float-right delete";
      deleteButtonEl.textContent = "X";
      listEl.appendChild(deleteButtonEl);
      listGroupEl.appendChild(listEl);
    }
  }
}

//deleting an item in the list

listGroupEl.addEventListener("click", removelistEl);

function removelistEl(e) {
  let li = e.target.parentNode;
  if (e.target.classList.contains("delete")) {
    if (confirm("Are you sure")) {
      listGroupEl.removeChild(li);
    }
  }

  let textContentOfItem = e.target.parentNode.firstChild.textContent;
  for (let index = 0; index < data.length; index++) {
    if (data[index].value == textContentOfItem) {
      data.splice(index, 1);
    }
  }

  //store data in localStorage
  localStorage.setItem("data", JSON.stringify(data));
}

//fitering an item in the list

filterEl.addEventListener("keyup", filterItems);

function filterItems(e) {
  let text = filterEl.value.toLowerCase();
  items = listGroupEl.getElementsByTagName("li");
  items = Array.from(items);
  items.forEach((element) => {
    let itemName = element.firstChild.textContent.toLowerCase();
    if (itemName.indexOf(text)) {
      element.style.display = "none";
    } else {
      element.style.display = "block";
    }
  });
}
